//
//  Copyright © 2017 fdapps. All rights reserved.
//

/**
 Http client interface to manage event using handle() method.
 */
public protocol IHttpClient {
    
    /**
     Handle event througth delegate when response is available.
     
     - Parameter event: event containing url, status code and response data.
     - Parameter failed: flag indicating if response is a failure.
     */
    func handle(event: HttpClientEvent, failed: Bool)
}
