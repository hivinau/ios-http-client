//
//  Copyright © 2017 fdapps. All rights reserved.
//

/**
 Methods utility to manage dispatch closure asynchronously.
 */
public class Thread {
    
    /**
     Run closure on main thread
     
     - parameter closure: closure to run on main thread
     */
    @available(iOS 9.0, *)
    public static func runOnUiThread(_ closure:@escaping ()->()) {
        
        #if swift(>=3.0)
            
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                
                DispatchQueue.main.async(execute: closure)
            }
            
        #elseif swift(>=2.2)
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                
                dispatch_async(dispatch_get_main_queue(), closure)
            }
            
        #endif
    }
}
