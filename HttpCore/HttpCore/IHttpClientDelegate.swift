//
//  Copyright © 2017 fdapps. All rights reserved.
//

/**
 Http client protocol to prevent if client succeed or failed while requesting to server.
 */
@objc public protocol IHttpClientDelegate {
    
    /**
     Event fired when server response is OK.
     
     - Parameter event: event containing url, status code and response data.
     */
    func onSuccess(event: HttpClientEvent)
    
    /**
     Event fired when server response is failed.
     
     - Parameter event: event containing url, status code and response data.
     */
    func onFailure(event: HttpClientEvent)
    
    /**
     Modify request timout.
     
     - returns: a time interval as second
     */
    @objc optional func timeout() -> TimeInterval
}
