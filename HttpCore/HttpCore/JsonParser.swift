//
//  Copyright © 2017 fdapps. All rights reserved.
//

/**
 Methods utility to manage JSON format.
 */
public class JsonParser {
    
    /**
     Parse data to JSON string representation
     
     - parameter data: data to parse
     
     - returns: JSON string representation of data as Array or Dictionnary
     
     - throws: NSError or Exception if parse failed
     */
    public static func parseData(data: Data?) throws -> Any {
        
        var object: Any!
        if let data = data {
            
            if let response = NSString(data: data, encoding: String.Encoding.utf8.rawValue) as String? {
                
                if let data = response.data(using: String.Encoding.utf8, allowLossyConversion: false) {
                    
                    do {
                        
                        let content = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                        if content is [AnyObject] {
                            
                            if let array = content as? [AnyObject] {
                                
                                object = array
                            }
                            
                        } else {
                            
                            if let dictionary = content as? [String:AnyObject] {
                                
                                object = dictionary
                            }
                        }
                        
                    } catch let error as NSError {
                        
                        throw error
                    }
                } else {
                    
                    throw HttpClient.Exception.DataRepresentationOfStringFailed
                }
                
            } else {
                
                throw HttpClient.Exception.DataRepresentationOfStringFailed
            }
            
        } else {
            
            throw HttpClient.Exception.DataNotValid
        }

        return object
    }
}
