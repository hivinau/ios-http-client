//
//  Copyright © 2017 fdapps. All rights reserved.
//

public extension HttpClient {
    
    /**
     Status of response as code.
     */
    public class ResponseCode {
        
        /// Standard response for successful HTTP requests.
        public static let OK = 200;
        
        /// The request has been fulfilled, resulting in the creation of a new resource.
        public static let CREATED = 201;
        
        /// The request has been accepted for processing, but the processing has not been completed.
        public static let ACCEPTED = 202;
        
        /// The server is a transforming proxy (e.g. a Web accelerator) that received a 200 OK from its origin, but is returning a modified version of the origin's response.
        public static let NON_AUTHORITATIVE_INFORMATION = 203;
        
        /// The server successfully processed the request and is not returning any content.
        public static let NO_CONTENT = 204;
        
        /// The server successfully processed the request, but is not returning any content. Unlike a 204 response, this response requires that the requester reset the document view.[
        public static let RESET_CONTENT = 205;
        
        /// The server is delivering only part of the resource (byte serving) due to a range header sent by the client.
        public static let PARTIAL_CONTENT = 206;
        
        /// The message body that follows is an XML message and can contain a number of separate response codes, depending on how many sub-requests were made.
        public static let MULTI_STATUS = 207;
        
        /// The members of a DAV binding have already been enumerated in a previous reply to this request, and are not being included again.
        public static let ALREADY_REPORTED = 208;
        
        /// The server has fulfilled a request for the resource, and the response is a representation of the result of one or more instance-manipulations applied to the current instance.
        public static let IM_USED = 226;
        
        /// The 520 error is used as a "catch-all response for when the origin server returns something unexpected", listing connection resets, large headers, and empty or invalid responses as common triggers.
        public static let UNKNOWN_ERROR = 520;
    }
}
