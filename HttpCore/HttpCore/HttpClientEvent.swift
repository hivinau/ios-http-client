//
//  Copyright © 2017 fdapps. All rights reserved.
//

/**
 Http client event.
 */
public class HttpClientEvent: NSObject {
    
    /// Request url.
    public private(set) var url: URL?
    
    /// Response code.
    public private(set) var statusCode: Int
    
    /// Response data.
    public private(set) var response: Any?
    
    /**
     Create a new instance of HttpClientEvent to compact url, response code and data.
     
     - Parameter url: request url.
     - Parameter statusCode: response code.
     - Parameter response: response data.
     */
    public init(url: URL, statusCode: Int, response: Any) {
    
        self.url = url
        self.statusCode = statusCode
        self.response = response
    }
}
