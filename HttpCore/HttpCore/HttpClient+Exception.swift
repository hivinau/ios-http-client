//
//  Copyright © 2017 fdapps. All rights reserved.
//

public extension HttpClient {
    
    /**
     Enum utility to manage Exception.
     */
    public enum Exception: Error {
        
        case DataRepresentationOfStringFailed
        case StringConversionOfCollectionFailed
        case DataNotValid
    }
}
