//
//  Copyright © 2017 fdapps. All rights reserved.
//

/**
 Http client protocol.
 */
@objc public protocol HttpClientDelegate: IHttpClientDelegate {
    
    /**
     Event fired when request is ready to be sent.
     */
    @objc optional func onRequestPrepare()
}
