//
//  Copyright © 2017 fdapps. All rights reserved.
//

/**
 Http client to I/O communicate using webservices.
 
 ```
 
 //Create HttpClient instance:
 let httpClient = HttpClient(url: "www.example.org/json", requestMethod: HttpClient.GET)
 
 //Add request parameters:
 httpClient.requestParameter(key: "id", value: "1")
           .requestParameter(key: <key>, value: <value>)
           .<some parameters to add if needed>
 
 //Set http request headers:
 httpClient.requestHeader(key: "Content-Type", value: "application/x-www-form-urlencoded")
           .requestHeader(key: <key>, value: <value>)
           .<some headers to add if needed>
 
 //Delegate implementation:
 // N.B: onSuccess() & onFailure() must be implemented 
 httpClient.delegate = self
 
 //Send request to server:
 httpClient.sendMessage()
 ```
 
 */
public class HttpClient: NSObject, IHttpClient {
    
    /// Delegate to manage http tasks asynchronously.
    public var delegate: HttpClientDelegate?
    
    /// GET request method.
    public static let GET = 0x01
    
    /// POST request method.
    public static let POST = 0x02
    
    /// request time interval as timeout.
    public static let REQUEST_TIMEOUT = 10 as TimeInterval
    
    private var url: String
    private let requestMethod: Int
    private var parameters: [String: String]
    private var headers: [String: String]
    
    /**
     Create a HttpClient instance.
     
     - Parameter url: host url to ask.
     - Parameter requestMethod: http request method.
     */
    public init(url: String, requestMethod: Int) {
        
        self.url = url
        self.requestMethod = requestMethod
        self.parameters = [String: String]()
        self.headers = [String: String]()
    }
    
    public func handle(event: HttpClientEvent, failed: Bool) {
        
        if let delegate = delegate {
            
            Thread.runOnUiThread {
                
                if failed {
                    
                    delegate.onFailure(event: event)
                    
                } else {
                    
                    delegate.onSuccess(event: event)
                }
            }
        }
    }
    
    /**
     Add request parameter.
     
     - Parameter key: request parameter key.
     - Parameter value: request parameter value.
     
     - Returns: HttpClient instance.
     */
    @discardableResult public func requestParameter(key: String, value: String) -> HttpClient {
        
        self.parameters[key] = value
        return self
    }
    
    /**
     Add request header.
     
     - Parameter key: http header field.
     - Parameter value: value of http header field.
     
     - Returns: HttpClient instance.
     */
    @discardableResult public func requestHeader(key: String, value: String) -> HttpClient {
        
        self.headers[key] = value
        return self
    }
    
    /**
     Send request message to server.
     */
    public func sendMessage() {
        
        var parametersString = ""
        
        if parameters.count > 0 {
            
            for parameter in parameters {
                
                parametersString += "\(parameter.key)=\(parameter.value)&"
            }
            
            if parametersString.hasSuffix("&") {
                
                let _ = parametersString.remove(at: parametersString.index(before: parametersString.endIndex))
            }
            
            if requestMethod == HttpClient.GET && parametersString.characters.count > 0 {
                
                self.url += "?\(parametersString)"
            }
        }
        
        if let url = URL(string: self.url) {
            
            var request = URLRequest(url: url)
            
            request.httpMethod = (requestMethod == HttpClient.GET) ? "GET" : "POST"
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            
            if headers.count > 0 {
                
                for header in headers {
                    
                    request.setValue(header.value, forHTTPHeaderField: header.key)
                }
            }
            
            if requestMethod == HttpClient.POST && parametersString.characters.count > 0 {
                
                let modifiedHostCharacterSet = (CharacterSet.urlHostAllowed as NSCharacterSet).mutableCopy() as! NSMutableCharacterSet
                modifiedHostCharacterSet.removeCharacters(in: " -[]+?.,")
                
                if let encoded = parametersString.addingPercentEncoding(withAllowedCharacters: modifiedHostCharacterSet as CharacterSet) {
                    
                    request.httpBody = encoded.data(using: String.Encoding.utf8)
                }
            }
            
            if let delegate = delegate {
                
                if let onRequestPrepare = delegate.onRequestPrepare {
                    
                    onRequestPrepare()
                }
            }
            
            send(request: request) {
                (data: Data?, code: URLResponse?, error: Error?) -> Void in
                
                var statusCode = HttpClient.ResponseCode.UNKNOWN_ERROR
                if let code = code as? HTTPURLResponse {
                    
                    statusCode = code.statusCode
                }
                
                if let url = URL(string: self.url) {
                    
                    if let error = error {
                        
                        let event = HttpClientEvent(url: url, statusCode: statusCode, response: error)
                        self.handle(event: event, failed: true)
                        
                    } else if let data = data {
                        
                        do {
                            
                            let object = try JsonParser.parseData(data: data)
                            
                            let event = HttpClientEvent(url: url, statusCode: statusCode, response: object)
                            
                            if statusCode != HttpClient.ResponseCode.OK &&
                                statusCode != HttpClient.ResponseCode.CREATED &&
                                statusCode != HttpClient.ResponseCode.ACCEPTED &&
                                statusCode != HttpClient.ResponseCode.NON_AUTHORITATIVE_INFORMATION &&
                                statusCode != HttpClient.ResponseCode.NO_CONTENT &&
                                statusCode != HttpClient.ResponseCode.RESET_CONTENT &&
                                statusCode != HttpClient.ResponseCode.PARTIAL_CONTENT &&
                                statusCode != HttpClient.ResponseCode.MULTI_STATUS &&
                                statusCode != HttpClient.ResponseCode.ALREADY_REPORTED &&
                                statusCode != HttpClient.ResponseCode.IM_USED {
                                
                                self.handle(event: event, failed: true)
                                
                            } else {
                                
                                self.handle(event: event, failed: false)
                            }
                            
                        } catch let exception as Exception {
                            
                            let event = HttpClientEvent(url: url, statusCode: statusCode, response: exception)
                            self.handle(event: event, failed: true)
                            
                        } catch let error as NSError {
                            
                            let event = HttpClientEvent(url: url, statusCode: statusCode, response: error)
                            self.handle(event: event, failed: true)
                        }
                        
                    } else {
                        
                        // FIXME: unimplemented part.
                    }
                }
            }
        }
    }
}

extension HttpClient: URLSessionDelegate, URLSessionTaskDelegate {
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if let serverTrust = challenge.protectionSpace.serverTrust {
            
            completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: serverTrust))
            
        } else if challenge.previousFailureCount > 0 {
            
            completionHandler(URLSession.AuthChallengeDisposition.cancelAuthenticationChallenge, nil)
        }
    }
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        
        completionHandler(request)
    }
    
    /**
     Send request to server.
     
     - Parameter request: request to send.
     - Parameter completion: completion while response is received from server containing data, response and error if exist.
     */
    internal func send(request: URLRequest, completion: ((Data?, URLResponse?, Error?) -> Void)?) {
        
        var interval = HttpClient.REQUEST_TIMEOUT
        
        if let delegate = delegate {
            
            if let timeout = delegate.timeout {
                
                interval = timeout()
            }
        }
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = interval
        
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
        
        let requestTask = session.dataTask(with: request) {
            (data: Data?, code: URLResponse?, error: Error?) -> Void in
            
            if let completion = completion {
                
                completion(data, code, error)
            }
        }
        
        requestTask.resume()
    }
}
