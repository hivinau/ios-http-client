//
//  Copyright © 2017 fdapps. All rights reserved.
//

import XCTest
@testable import HttpCore

public class HttpCoreTests: XCTestCase, HttpClientDelegate {
    
    private static let TIMEOUT = 5 as TimeInterval
    
    private var expectation: XCTestExpectation?
    
    private var host: String?
    private var statusCode: Int?
    private var response: Any?
    
    public func testHttpGetRequestSucceed() {
        
        let host = "http://httpbin.org/get"
        
        httpRequest(host: host, requestMethod: HttpClient.GET, delegate: self)
        
        XCTAssertEqual(statusCode, HttpClient.ResponseCode.OK)
        XCTAssertEqual(self.host, host)
        XCTAssertNotNil(response)
    }
    
    public func testHttpPostRequestSucceed() {
        
        let host = "https://initiatice.loganlepage.fr/api/user/auth"
        
        httpRequest(host: host, requestMethod: HttpClient.POST, delegate: self, parameters: ["email": "test1@hotmail.fr", "plainPassword": "12345678"])
        
        XCTAssertEqual(statusCode, HttpClient.ResponseCode.OK)
        XCTAssertEqual(self.host, host)
        XCTAssertNotNil(response)
    }
    
    public func testHttpGetRequestFailed() {
        
        let host = "http://httpbin.org/gets" //fake route
        
        httpRequest(host: host, requestMethod: HttpClient.GET, delegate: self)
        
        XCTAssertNotEqual(statusCode, HttpClient.ResponseCode.OK)
        XCTAssertEqual(self.host, host)
        XCTAssertNotNil(response)
    }
    
    public func testHttpPostRequestFailed() {
        
        let host = "https://initiatice.loganlepage.fr/api/user/auth"
        
        httpRequest(host: host, requestMethod: HttpClient.POST, delegate: self, parameters: ["email": "test1@hotmail.fr"]) //missing plainPassword field
        
        XCTAssertNotEqual(statusCode, HttpClient.ResponseCode.OK)
        XCTAssertEqual(self.host, host)
        XCTAssertNotNil(response)
    }
    
    public func onSuccess(event: HttpClientEvent) {
        
        fillExpectation(event: event)
    }
    
    public func onFailure(event: HttpClientEvent) {
        
        fillExpectation(event: event)
    }
    
    private func httpRequest(host: String, requestMethod: Int, delegate: HttpClientDelegate, parameters: [String:String]? = nil) {
        
        let httpClient = HttpClient(url: host, requestMethod: requestMethod)
        httpClient.delegate = delegate
        
        if let parameters = parameters, parameters.count > 0 {
            
            for parameter in parameters {
                
                httpClient.requestParameter(key: parameter.key, value: parameter.value)
            }
        }
        
        expectation = expectation(description: "Waiting server response.")
        
        httpClient.sendMessage()
        
        waitForExpectations(timeout: HttpCoreTests.TIMEOUT, handler: nil)
        
    }
    
    private func fillExpectation(event: HttpClientEvent) {
        
        host = event.url?.absoluteString
        statusCode = event.statusCode
        response = event.response
        
        expectation?.fulfill()
    }

    
}
